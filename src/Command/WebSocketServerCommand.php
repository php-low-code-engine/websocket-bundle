<?php

namespace PhpLowCodeEngine\WebsocketBundle\Command;

use App\Entity\NodeExecution;
use PhpLowCodeEngine\WebsocketBundle\Services\EventDispatcher\WebsocketNodeExecutionFinishedEvent;
use Doctrine\ORM\EntityManagerInterface;
use PhpLowCodeEngine\WebsocketBundle\Services\EventDispatcher\WebsocketSendMessage;
use PhpLowCodeEngine\WebsocketBundle\Services\Session\SessionProvider;
use PhpLowCodeEngine\WebsocketBundle\Services\Websocket\WsRouteFactory;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Ratchet\Http\HttpServer;
use Ratchet\Server\IoServer;
use Ratchet\WebSocket\WsServer;
use React\EventLoop\Loop;
use React\Socket\SocketServer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Session\SessionFactoryInterface;
use Symfony\Component\Messenger\Transport\TransportInterface;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\RouteCollection;

class WebSocketServerCommand extends Command
{
    private RouteCollection $routes;

    public function __construct(
        private readonly LoggerInterface          $logger,
        private readonly EventDispatcherInterface $eventDispatcher,
        private readonly ContainerInterface       $receiverLocator,
        private readonly EntityManagerInterface   $entityManager,
        private readonly SessionFactoryInterface  $sessionFactory,
        private readonly WsRouteFactory           $wsRouteFactory
    )
    {
        parent::__construct('websocket:server');

        $this->routes = new RouteCollection();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->logger->info('Start WebSocket server', ['method' => __METHOD__]);
        $this->startWsServer();

        return Command::SUCCESS;
    }

    public function startWsServer():void
    {
        $this->updateRoutes();

        if (!$loop = Loop::get()) {
            $this->logger->error('Error create loop', ['method' => __METHOD__]);
            return;
        }

        if (extension_loaded('pcntl')) {
            if (!function_exists('pcntl_signal')) {
                throw new \ErrorException(
                    "Function 'pcntl_signal' is referenced in the php.ini 'disable_functions' and can't be called."
                );
            }

            $stopServer = function () use ($loop) {
                $loop->stop();
            };

            pcntl_signal(SIGTERM, $stopServer);
            pcntl_signal(SIGINT, $stopServer);
            pcntl_signal(SIGHUP, $stopServer);
        }

        $socket = new SocketServer(getenv('WEBSOCKET_URI'), [], $loop);

        $component = new HttpServer(
            new WsServer(
                new SessionProvider(
                    new \Ratchet\Http\Router(
                        new UrlMatcher($this->routes, new RequestContext)
                    ),
                    $this->sessionFactory,
                    $this->logger
                )
            )
        );

        $server = new IoServer($component, $socket, $loop);

        $loop->addPeriodicTimer(0.1, [$this, 'receiveExecutionFinish']);
        $loop->addPeriodicTimer(10, [$this, 'updateRoutes']);

        $this->logger->info('Start websocker server', ['method' => __METHOD__]);

        $server->run();
    }

    public function updateRoutes()
    {
        $this->wsRouteFactory->updateRoutes($this->routes);
    }

    public function receiveExecutionFinish()
    {
        /** @var TransportInterface $receiver */
        $receiver = $this->receiverLocator->get('execution_finish');

        foreach ($receiver->get() as $envelope) {

            /** @var WebsocketSendMessage $message */
            $message = $envelope->getMessage();
            $this->logger->debug('Received message from execution_finish by loop', ['message' => $message, 'method' => __METHOD__]);
            $nodeExecution = $this->entityManager->find(NodeExecution::class, $message->getNodeExecutionId());

            if (!$nodeExecution) {
                $this->logger->error('NodeExecution not found', ['node_execution_id' => $message->getNodeExecutionId(), 'method' => __METHOD__]);
                $receiver->ack($envelope);
                continue;
            }

            $this->eventDispatcher->dispatch(
                new WebsocketNodeExecutionFinishedEvent($nodeExecution),
                WebsocketNodeExecutionFinishedEvent::NAME
            );

            $receiver->ack($envelope);
        }
    }
}
