<?php

namespace PhpLowCodeEngine\WebsocketBundle;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class WebsocketBundle extends \Symfony\Component\HttpKernel\Bundle\AbstractBundle
{
    public function build(ContainerBuilder $container): void
    {
        parent::build($container);

        $loader = new YamlFileLoader($container, new FileLocator(__DIR__.'/Resources/config'));
        $loader->load('services.yaml');
        $loader->load('messenger.yaml');
    }

//    public function loadExtension(array $config, ContainerConfigurator $container, ContainerBuilder $builder): void
//    {
//        $container->import(__DIR__ . '/Resources/config/services.yaml');
//        $container->import(__DIR__ . '/Resources/config/messenger.yaml');
//    }
}
