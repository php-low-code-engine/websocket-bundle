<?php

namespace PhpLowCodeEngine\WebsocketBundle\Services\Websocket\Controllers;

use App\Entity\Node;
use App\Entity\Workflow;
use App\Services\Execution\ExecutionService;
use PhpLowCodeEngine\WebsocketBundle\Services\EventDispatcher\WebsocketNodeExecutionFinishedEvent;
use Doctrine\ORM\EntityManagerInterface;
use PhpLowCodeEngine\WebsocketBundle\Services\NodeType\WebsocketTriggerNode;
use PhpLowCodeEngine\WebsocketBundle\WsException;
use Psr\Http\Message\RequestInterface;
use Psr\Log\LoggerInterface;
use Ratchet\Http\CloseResponseTrait;
use Ratchet\Http\HttpServerInterface;
use Ratchet\ConnectionInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Serializer\SerializerInterface;

class WebsocketController implements HttpServerInterface
{
    use CloseResponseTrait;
    private $connections;

    public function __construct(
        private readonly Node $node,
        private readonly EventDispatcherInterface $eventDispatcher,
        private readonly ExecutionService $executionService,
        private readonly EntityManagerInterface $entityManager,
        private readonly LoggerInterface $logger,
        private readonly SerializerInterface $serializer
    )
    {
        $this->connections = new \SplObjectStorage;

        $this->eventDispatcher->addListener(WebsocketNodeExecutionFinishedEvent::NAME,
            function (WebsocketNodeExecutionFinishedEvent $event) {
                $nodeExecution = $event->getNodeExecution();
                $execution = $nodeExecution->getExecution();

                $connection = null;
                foreach ($this->connections as $connectionItem) {
                    if ($connectionItem->Session->getId() === $execution->getSessionId()) {
                        $connection = $connectionItem;
                        break;
                    }
                }
                if (!$connection) {
                    $this->logger->warning("Not found ws connection", [
                        'execution.session_id' => $execution->getSessionId(),
                        'method' => __METHOD__
                    ]);
                    return;
                }

                $outputData = $nodeExecution->getOutputData();

                $this->logger->debug("Finished execution {$execution->getId()}", [
                    'execution.session_id' => $execution->getSessionId(),
                    'nodeLastExecution' => $nodeExecution->getId(),
                    'outputData' => $outputData->toArray(),
                    'method' => __METHOD__,
                ]);

                $jsonData = $this->serializer->serialize($outputData->toArray(), 'json');

                $connection->send($jsonData);
        });
    }

    public function onOpen(ConnectionInterface $conn, RequestInterface $request = null)
    {
        if (!$this->checkSecurityToken($request)) {
            $this->close($conn, 403);
            return;
        }

        if (!in_array('onOpen', $this->node->getParam('enents'))) {
            return;
        }

        $this->logger->debug("Established ws connection", [
            'uri' => $request->getUri(),
            'IP' => $conn->remoteAddress,
            'session_id' => $conn->Session->getId(),
            'method' => __METHOD__
        ]);

        $this->connections->attach($conn);

        $this->startExecution($conn, 'onOpen');
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        if (!in_array('onMessage', $this->node->getParam('enents'))) {
            return;
        }

        $this->startExecution($from, 'onMessage', $msg);
    }

    public function onClose(ConnectionInterface $conn)
    {
        if (!in_array('onClose', $this->node->getParam('enents'))) {
            return;
        }

        $this->startExecution($conn, 'onMessage');
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        if (!in_array('onError', $this->node->getParam('enents'))) {
            return;
        }

        $this->logger->error("Error ws connection", [
            'exception' => $e,
            'method' => __METHOD__
        ]);

        $this->startExecution($conn, 'onError', $e->getMessage());
    }

    public function startExecution(ConnectionInterface $from, string $event, string $msg = '')
    {
        $workflow = $this->entityManager->find(Workflow::class, $this->node->getWorkflowId());

        $websocketNodes = $workflow->getNodes()->filter(function (Node $node) {
            return $node->getType() === WebsocketTriggerNode::class;
        });

        if ($websocketNodes->isEmpty()) {
            throw new WsException('Not found websocket trigger node');
        }

        $execution = $this->executionService->createExecution($workflow, $websocketNodes->first(), [
            'message' => $msg,
            'session_id' => $from->Session->getId(),
            'event' => $event
        ]);

        $this->executionService->runExecutionAsync($execution);

    }

    private function checkSecurityToken(?RequestInterface $request):bool
    {
        if (!$request) {
            $this->logger->debug("Access denied", [
                'reason' => 'request is null',
                'method' => __METHOD__
            ]);
            return false;
        }

        $parsedUrl = parse_url($request->getUri());
        $queryString = $parsedUrl['query'] ?? '';
        parse_str($queryString, $queryParams);
        $token = isset($queryParams['token']) ? $queryParams['token'] : null;

        if (empty($token) or ($token !== $this->node->getParam('token'))) {
            $this->logger->debug("Access denied", [
                'reason' => 'wrong token',
                'token' => $token,
                'method' => __METHOD__
            ]);
            return false;
        }

        return true;
    }
}

