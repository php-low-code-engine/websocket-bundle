<?php

namespace PhpLowCodeEngine\WebsocketBundle\Services\Websocket;

use Symfony\Component\HttpFoundation\Session\SessionInterface;

/**
 * @property SessionInterface Session
 */
class WsConnection extends \Ratchet\WebSocket\WsConnection
{

}
