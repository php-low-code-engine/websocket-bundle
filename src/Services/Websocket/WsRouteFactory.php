<?php

namespace PhpLowCodeEngine\WebsocketBundle\Services\Websocket;

use App\Entity\Node;
use App\Repository\NodeRepository;
use App\Services\Execution\ExecutionService;
use Doctrine\ORM\EntityManagerInterface;
use PhpLowCodeEngine\WebsocketBundle\Services\NodeType\WebsocketTriggerNode;
use PhpLowCodeEngine\WebsocketBundle\Services\Websocket\Controllers\WebsocketController;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Routing\Route;
use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Serializer\SerializerInterface;

class WsRouteFactory
{
    public function __construct(
        private readonly EventDispatcherInterface $eventDispatcher,
        private readonly ExecutionService $executionService,
        private readonly EntityManagerInterface $entityManager,
        private readonly LoggerInterface          $logger,
        private readonly SerializerInterface $serializer
    )
    {}


    public function updateRoutes(RouteCollection $routes): void
    {
        $activeWebsocketTriggerableNodes = $this->getActiveWebsocketTriggerableNodes();

        foreach ($activeWebsocketTriggerableNodes as $activeWebsocketTriggerableNode) {
            $this->addRoute($routes, $activeWebsocketTriggerableNode);
        }

        $this->logger->debug('Updated ws routes', ['method' => __METHOD__, 'routes' => array_keys($routes->all())]);
    }

    private function addRoute(RouteCollection $routes, Node $node): void
    {
        $path = $node->getParam('path');
        $name = trim($path, '/');

        if ($routes->get($name)) {
            $this->logger->debug('Route has already exists', ['name' => $name, 'method' => __METHOD__]);
            return;
        }

        $path = '/' . $name;
        $routes->remove($name);

        $controller = new WebsocketController(
            $node,
            $this->eventDispatcher,
            $this->executionService,
            $this->entityManager,
            $this->logger,
            $this->serializer
        );

        $route = new Route($path, ['_controller' => $controller]);
        $routes->add($name, $route);
    }


    /**
     * @return Node[]
     */
    private function getActiveWebsocketTriggerableNodes(): array
    {
        $activeTriggerableNodes = $this->entityManager->getRepository(Node::class)->getActiveTriggerableNodes();
        return array_filter($activeTriggerableNodes, function (Node $node) {
            return $node->getType() === WebsocketTriggerNode::class;
        });
    }
}
