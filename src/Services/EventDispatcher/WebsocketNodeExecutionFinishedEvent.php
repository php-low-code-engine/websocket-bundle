<?php

namespace PhpLowCodeEngine\WebsocketBundle\Services\EventDispatcher;


use App\Entity\Execution;
use App\Entity\NodeExecution;

class WebsocketNodeExecutionFinishedEvent
{
    const NAME = 'ws.execution.finished';

    public function __construct(
        private readonly NodeExecution $nodeExecution
    )
    {
    }

    /**
     * @return Execution
     */
    public function getNodeExecution(): NodeExecution
    {
        return $this->nodeExecution;
    }
}
