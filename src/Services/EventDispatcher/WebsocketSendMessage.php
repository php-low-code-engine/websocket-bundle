<?php

namespace PhpLowCodeEngine\WebsocketBundle\Services\EventDispatcher;

class WebsocketSendMessage
{
    public function __construct(
        private readonly string $node_execution_id
    )
    {
    }

    public function getNodeExecutionId(): string
    {
        return $this->node_execution_id;
    }
}
