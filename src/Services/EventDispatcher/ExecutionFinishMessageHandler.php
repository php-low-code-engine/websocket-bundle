<?php

namespace PhpLowCodeEngine\WebsocketBundle\Services\EventDispatcher;

use App\Entity\Execution;
use App\Entity\NodeExecution;
use App\Services\Trigger\Event\ExecutionFinishedEvent;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\MessageBusInterface;

//#[AsMessageHandler]
class ExecutionFinishMessageHandler
{
    public function __construct(
        private readonly EventDispatcherInterface $eventDispatcher,
        private readonly EntityManagerInterface $entityManager
    )
    {
    }

    public function __invoke(WebsocketSendMessage $message): void
    {
        $nodeExecution = $this->entityManager->find(NodeExecution::class, $message->getNodeExecutionId());

        $this->eventDispatcher->dispatch(
            new WebsocketNodeExecutionFinishedEvent($nodeExecution),
            WebsocketNodeExecutionFinishedEvent::NAME
        );
    }
}
