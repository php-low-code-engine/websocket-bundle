<?php

namespace PhpLowCodeEngine\WebsocketBundle\Services\NodeType;

use App\Entity\Node;
use App\Entity\NodeExecution;
use App\Services\Trigger\TriggerableNodeInterface;
use App\Validator\Constraints\NodeParams;
use EasyCorp\Bundle\EasyAdminBundle\Field\ChoiceField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class WebsocketTriggerNode implements \App\Services\NodeType\Types\NodeTypeInterface, \App\Services\Execution\ExecutionNodeInterface, TriggerableNodeInterface
{

    public function run(NodeExecution $nodeExecution): string
    {
        $nodeExecution->setOutputData($nodeExecution->getInputData()->toArray());

        return self::DEFAULT_RESULT;
    }

    static public function getName(): string
    {
        return 'Websocket триггер';
    }

    public function configureFields(Node $node): iterable
    {
        $choices = [
            'onOpen' => 'onOpen',
            'onMessage' => 'onMessage',
            'onClose' => 'onClose',
            'onError' => 'onError',
        ];
        yield ChoiceField::new('enents')
            ->setChoices($choices)->allowMultipleChoices(true)
            ->setValue($node->getParam('enents') ?? null);

        yield TextField::new('path')->setValue($node->getParam('path') ?? null);
        yield TextField::new('token')->setValue($node->getParam('token') ?? null);

    }

    public function validateParams(array $params, NodeParams $constraint, ExecutionContextInterface $context)
    {
        // TODO: Implement validateParams() method.
    }

    public function addTrigger(Node $node, string $type)
    {
        // The trigger is added automatically when the websocket server starts
    }

    public function removeTrigger(Node $node, string $type)
    {
        // The trigger is added automatically when the websocket server stops
    }
}
