<?php

namespace PhpLowCodeEngine\WebsocketBundle\Services\NodeType;

use App\Entity\Node;
use App\Entity\NodeExecution;
use App\Field\AutocomleteTextareaField;
use App\Services\Execution\ExecutionNodeInterface;
use App\Services\NodeType\Types\NodeTypeInterface;
use App\Services\Template\TemplateServiceInterface;
use App\Validator\Constraints\NodeParams;
use Doctrine\ORM\EntityManagerInterface;
use PhpLowCodeEngine\WebsocketBundle\Services\EventDispatcher\WebsocketSendMessage;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class WebsocketSendNode implements NodeTypeInterface, ExecutionNodeInterface
{
    public function __construct(
        private readonly MessageBusInterface $messageBus,
        private readonly EntityManagerInterface $entityManager,
        private readonly TemplateServiceInterface $templateService
    )
    {
    }

    public function run(NodeExecution $nodeExecution): string
    {
        $node = $nodeExecution->getNode();
        $message = $this->templateService->render($node->getParam('message'), $nodeExecution->getInputData());

        $nodeExecution->setOutputData(array_merge(
            $nodeExecution->getInputData()->toArray(),
            ['message' => $message]
        ));

        $this->entityManager->persist($nodeExecution);
        $this->entityManager->flush();

        $this->messageBus->dispatch(new WebsocketSendMessage($nodeExecution->getId()));
        return ExecutionNodeInterface::DEFAULT_RESULT;
    }

    static public function getName(): string
    {
        return 'Websocket send';
    }

    public function configureFields(Node $node): iterable
    {
        yield AutocomleteTextareaField::new('message')
            ->setValue($node->getParam('message') ?? null);
    }

    public function validateParams(array $params, NodeParams $constraint, ExecutionContextInterface $context)
    {
        // TODO: Implement validateParams() method.
    }
}
