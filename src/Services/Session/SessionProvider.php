<?php

namespace PhpLowCodeEngine\WebsocketBundle\Services\Session;

use Psr\Http\Message\RequestInterface;
use Psr\Log\LoggerInterface;
use Ratchet\ConnectionInterface;
use Ratchet\Http\HttpServerInterface;
use Ratchet\WebSocket\WsConnection;
use Symfony\Component\HttpFoundation\Session\SessionFactoryInterface;

class SessionProvider implements HttpServerInterface
{

    public function  __construct(
        private readonly HttpServerInterface $_app,
        private readonly SessionFactoryInterface $sessionFactory,
        private readonly LoggerInterface $logger
    )
    {
    }

    function onClose(ConnectionInterface $conn)
    {
        return $this->_app->onClose($conn);
    }

    function onError(ConnectionInterface $conn, \Exception $e)
    {
        return $this->_app->onError($conn, $e);
    }

    /**
     * @param ConnectionInterface|WsConnection $conn
     * @param RequestInterface|null $request
     * @return mixed
     */
    public function onOpen(ConnectionInterface $conn, RequestInterface $request = null)
    {
        if (null === $conn->httpRequest) {
            throw new \UnexpectedValueException('httpRequest can not be null');
        }

        $request = $conn->httpRequest;

        $conn->Session = $this->sessionFactory->createSession($request);

        $conn->Session->start();

        $this->logger->info('Started session', [
            'session_id' => $conn->Session->getId(),
        ]);

        $conn->Session->set('remoteAddress', $conn->remoteAddress);

        $cookie = new \Symfony\Component\HttpFoundation\Cookie(
            $conn->Session->getName(), // Название cookie
            $conn->Session->getId(),       // Значение cookie
            time() + (3600 * 24), // Время жизни cookie (1 день)
            '/',                // Путь
            'localhost',               // Домен (null означает использовать текущий домен)
            false,              // Использовать ли HTTPS только
            false,               // Только для HTTP (не доступно через JavaScript)
            false,              // Raw (если значение cookie не должно быть URL-encoded)
            'lax'               // Политика SameSite
        );

        $conn->send(json_encode(['cookie' => (string) $cookie]));

        $conn->Session->save();

        return $this->_app->onOpen($conn, $request);
    }

    function onMessage(ConnectionInterface $from, $msg)
    {
        return $this->_app->onMessage($from, $msg);
    }
}
